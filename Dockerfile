FROM openjdk:15-alpine
ADD build/libs/configuration-service.jar service.jar
EXPOSE 7500
ENTRYPOINT ["java", "-jar", "service.jar"]

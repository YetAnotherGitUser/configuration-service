package dev.ecosystem.configurationservice.persistence.entity;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;

class ConfigurationParamTest {

    private static final Long ID = 5L;
    private static final Long COUNTRY_ID = 10L;
    private static final Long USER_ID = 15L;
    private static final String KEY = "key";
    private static final String VALUE = "value";
    private static final OffsetDateTime CREATION_DATE = OffsetDateTime.now();
    private static final OffsetDateTime CHANGE_DATE = OffsetDateTime.now();


    @Test
    public void basicEntityTest() {
        ConfigurationParam configurationParam = new ConfigurationParam();
        configurationParam.setId(ID);
        configurationParam.setCountryId(COUNTRY_ID);
        configurationParam.setKey(KEY);
        configurationParam.setValue(VALUE);
        configurationParam.setCreationDate(CREATION_DATE);
        configurationParam.setChangeDate(CHANGE_DATE);
        configurationParam.setChangedBy(USER_ID);

        Truth.assertThat(configurationParam.getId()).isEqualTo(ID);
        Truth.assertThat(configurationParam.getCountryId()).isEqualTo(COUNTRY_ID);
        Truth.assertThat(configurationParam.getKey()).isEqualTo(KEY);
        Truth.assertThat(configurationParam.getValue()).isEqualTo(VALUE);
        Truth.assertThat(configurationParam.getCreationDate()).isEqualTo(CREATION_DATE);
        Truth.assertThat(configurationParam.getChangeDate()).isEqualTo(CHANGE_DATE);
        Truth.assertThat(configurationParam.getChangedBy()).isEqualTo(USER_ID);
    }
}
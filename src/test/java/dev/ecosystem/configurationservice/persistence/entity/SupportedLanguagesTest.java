package dev.ecosystem.configurationservice.persistence.entity;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;

class SupportedLanguagesTest {

    private static final Long ID = 5L;
    private static final Long COUNTRY_ID = 10L;
    private static final Long USER_ID = 15L;
    private static final String NAME = "name";
    private static final String CODE = "code";
    private static final OffsetDateTime CREATION_DATE = OffsetDateTime.now();
    private static final OffsetDateTime CHANGE_DATE = OffsetDateTime.now();


    @Test
    public void basicEntityTest() {
        SupportedLanguages supportedLanguages = new SupportedLanguages();
        supportedLanguages.setId(ID);
        supportedLanguages.setCountryId(COUNTRY_ID);
        supportedLanguages.setName(NAME);
        supportedLanguages.setCode(CODE);
        supportedLanguages.setCreationDate(CREATION_DATE);
        supportedLanguages.setChangeDate(CHANGE_DATE);
        supportedLanguages.setChangedBy(USER_ID);

        Truth.assertThat(supportedLanguages.getId()).isEqualTo(ID);
        Truth.assertThat(supportedLanguages.getCountryId()).isEqualTo(COUNTRY_ID);
        Truth.assertThat(supportedLanguages.getName()).isEqualTo(NAME);
        Truth.assertThat(supportedLanguages.getCode()).isEqualTo(CODE);
        Truth.assertThat(supportedLanguages.getCreationDate()).isEqualTo(CREATION_DATE);
        Truth.assertThat(supportedLanguages.getChangeDate()).isEqualTo(CHANGE_DATE);
        Truth.assertThat(supportedLanguages.getChangedBy()).isEqualTo(USER_ID);
    }
}
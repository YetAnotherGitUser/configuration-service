package dev.ecosystem.configurationservice.persistence.entity;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.util.Set;

class SupportedCountriesTest {

    private static final Long ID = 5L;
    private static final Long USER_ID = 15L;
    private static final String NAME = "name";
    private static final String OFFICIAL_NAME = "official_name";
    private static final String CODE = "code";
    private static final OffsetDateTime CREATION_DATE = OffsetDateTime.now();
    private static final OffsetDateTime CHANGE_DATE = OffsetDateTime.now();


    @Test
    public void basicEntityTest() {
        SupportedCountries supportedCountries = new SupportedCountries();
        Set<SupportedLanguages> languagesSet = Set.of(new SupportedLanguages());
        Set<ConfigurationParam> configurationParamSet = Set.of(new ConfigurationParam());

        supportedCountries.setId(ID);
        supportedCountries.setName(NAME);
        supportedCountries.setOfficialName(OFFICIAL_NAME);
        supportedCountries.setCode(CODE);
        supportedCountries.setLanguages(languagesSet);
        supportedCountries.setConfigurations(configurationParamSet);
        supportedCountries.setCreationDate(CREATION_DATE);
        supportedCountries.setChangeDate(CHANGE_DATE);
        supportedCountries.setChangedBy(USER_ID);

        Truth.assertThat(supportedCountries.getId()).isEqualTo(ID);
        Truth.assertThat(supportedCountries.getName()).isEqualTo(NAME);
        Truth.assertThat(supportedCountries.getOfficialName()).isEqualTo(OFFICIAL_NAME);
        Truth.assertThat(supportedCountries.getCode()).isEqualTo(CODE);
        Truth.assertThat(supportedCountries.getLanguages()).isEqualTo(languagesSet);
        Truth.assertThat(supportedCountries.getConfigurations()).isEqualTo(configurationParamSet);
        Truth.assertThat(supportedCountries.getCreationDate()).isEqualTo(CREATION_DATE);
        Truth.assertThat(supportedCountries.getChangeDate()).isEqualTo(CHANGE_DATE);
        Truth.assertThat(supportedCountries.getChangedBy()).isEqualTo(USER_ID);
    }
}
package dev.ecosystem.configurationservice.presentation.configs;

import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.Config;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.cached.GetConfigsCachedQuery;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.direct.GetConfigsQuery;
import dev.ecosystem.configurationservice.application.exceptions.DatabaseDataException;
import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Set;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConfigurationsControllerTest {

    private static final Long COUNTRY_ID = 123L;

    @InjectMocks
    private ConfigurationsController configurationsController;

    @Mock
    private GetConfigsCachedQuery getConfigsQuery;

    @Test
    public void testPositiveResponse() {
        Config config = new Config(new ConfigurationParam());
        Set<Config> configSet = Set.of(config);
        when(getConfigsQuery.execute(eq(COUNTRY_ID))).thenReturn(configSet);

        ResponseEntity<Set<Config>> controllerResponse = configurationsController.getConfigs(COUNTRY_ID);

        Truth.assertThat(controllerResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        Truth.assertThat(controllerResponse.getBody()).isEqualTo(configSet);
    }

    @Test
    public void testEntityNotFound() {
        when(getConfigsQuery.execute(eq(COUNTRY_ID))).thenThrow(DatabaseDataException.class);

        ResponseEntity<Set<Config>> controllerResponse = configurationsController.getConfigs(COUNTRY_ID);

        Truth.assertThat(controllerResponse.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        Truth.assertThat(controllerResponse.getBody()).isEqualTo(null);
    }
}
package dev.ecosystem.configurationservice.presentation.configs;

import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.Config;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.cached.GetConfigCachedQuery;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.direct.GetConfigQuery;

import dev.ecosystem.configurationservice.application.exceptions.NotFoundException;
import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConfigurationControllerTest {

    private static final Long COUNTRY_ID = 777L;

    private static final String PARAM_KEY = "KEY";

    @InjectMocks
    private ConfigurationController configurationController;

    @Mock
    private GetConfigCachedQuery getConfigQuery;

    @Test
    public void testPositiveResponse() {
        Config config = new Config(new ConfigurationParam());
        when(getConfigQuery.execute(eq(COUNTRY_ID), eq(PARAM_KEY))).thenReturn(config);

        ResponseEntity<Config> controllerResponse = configurationController.getConfig(COUNTRY_ID, PARAM_KEY);

        Truth.assertThat(controllerResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        Truth.assertThat(controllerResponse.getBody()).isEqualTo(config);
    }

    @Test
    public void testNotFoundEntity() {
        when(getConfigQuery.execute(eq(COUNTRY_ID), eq(PARAM_KEY))).thenThrow(NotFoundException.class);

        ResponseEntity<Config> controllerResponse = configurationController.getConfig(COUNTRY_ID, PARAM_KEY);

        Truth.assertThat(controllerResponse.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        Truth.assertThat(controllerResponse.getBody()).isEqualTo(null);
    }
}
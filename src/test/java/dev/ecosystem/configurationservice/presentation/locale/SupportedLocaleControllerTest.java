package dev.ecosystem.configurationservice.presentation.locale;

import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.application.countries.query.getcountries.Country;
import dev.ecosystem.configurationservice.application.countries.query.getcountries.cached.GetLocaleCachedQuery;
import dev.ecosystem.configurationservice.application.exceptions.DatabaseDataException;
import dev.ecosystem.configurationservice.persistence.entity.SupportedCountries;
import dev.ecosystem.configurationservice.persistence.entity.SupportedLanguages;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Set;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SupportedLocaleControllerTest {

    @InjectMocks
    private SupportedLocaleController supportedLocaleController;

    @Mock
    private GetLocaleCachedQuery getLocaleQuery;

    @Test
    public void testPositiveResponse() {
        SupportedCountries supportedCountries = new SupportedCountries();
        supportedCountries.setLanguages(Set.of(new SupportedLanguages()));
        Country country = new Country(supportedCountries);
        Set<Country> countrySet = Set.of(country);
        when(getLocaleQuery.execute()).thenReturn(countrySet);

        ResponseEntity<Set<Country>> controllerResponse = supportedLocaleController.getSupportedCountries();

        Truth.assertThat(controllerResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        Truth.assertThat(controllerResponse.getBody()).isEqualTo(countrySet);
    }

    @Test
    public void testEntityNotFound() {
        when(getLocaleQuery.execute()).thenThrow(DatabaseDataException.class);

        ResponseEntity<Set<Country>> controllerResponse = supportedLocaleController.getSupportedCountries();

        Truth.assertThat(controllerResponse.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        Truth.assertThat(controllerResponse.getBody()).isEqualTo(null);
    }
}
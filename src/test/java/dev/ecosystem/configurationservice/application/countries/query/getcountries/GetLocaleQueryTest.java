package dev.ecosystem.configurationservice.application.countries.query.getcountries;

import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.application.countries.query.getcountries.cached.GetLocaleCachedQuery;
import dev.ecosystem.configurationservice.application.countries.query.getcountries.direct.GetLocaleQuery;
import dev.ecosystem.configurationservice.application.exceptions.DatabaseDataException;
import dev.ecosystem.configurationservice.persistence.entity.SupportedCountries;
import dev.ecosystem.configurationservice.persistence.entity.SupportedLanguages;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Set;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GetLocaleQueryTest {

    @InjectMocks
    private GetLocaleCachedQuery getLocaleCachedQuery;

    @Mock
    private GetLocaleQuery getLocaleQuery;

    @Test
    public void testEmptyCacheQuery() {
        SupportedLanguages supportedLanguages = new SupportedLanguages();
        SupportedCountries supportedCountries = new SupportedCountries();
        supportedCountries.setLanguages(Set.of(supportedLanguages));
        Set<Country> countries = Set.of(new Country(supportedCountries));
        when(getLocaleQuery.executeFromCache()).thenReturn(null);
        when(getLocaleQuery.executeFromDatabase()).thenReturn(countries);

        Set<Country> response = getLocaleCachedQuery.execute();

        Truth.assertThat(response).hasSize(1);
        Country country = response.iterator().next();
        Truth.assertThat(country.getId()).isEqualTo(supportedCountries.getId());
        Truth.assertThat(country.getName()).isEqualTo(supportedCountries.getName());
        Truth.assertThat(country.getCode()).isEqualTo(supportedCountries.getCode());
        Truth.assertThat(country.getOfficialName()).isEqualTo(supportedCountries.getOfficialName());
        Truth.assertThat(country.getSupportedLanguages()).hasSize(1);
    }
}
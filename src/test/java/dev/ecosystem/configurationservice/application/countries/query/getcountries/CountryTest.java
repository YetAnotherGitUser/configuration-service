package dev.ecosystem.configurationservice.application.countries.query.getcountries;

import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.persistence.entity.SupportedCountries;
import dev.ecosystem.configurationservice.persistence.entity.SupportedLanguages;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

class CountryTest {

    @Test
    public void testConstructor() {
        SupportedLanguages supportedLanguages = new SupportedLanguages();
        supportedLanguages.setCode("a");

        SupportedCountries supportedCountries = new SupportedCountries();
        supportedCountries.setId(1L);
        supportedCountries.setName("name");
        supportedCountries.setCode("code");
        supportedCountries.setOfficialName("official name");
        supportedCountries.setLanguages(Set.of(supportedLanguages));

        Country country = new Country(supportedCountries);

        Truth.assertThat(country.getId()).isEqualTo(supportedCountries.getId());
        Truth.assertThat(country.getCode()).isEqualTo(supportedCountries.getCode());
        Truth.assertThat(country.getName()).isEqualTo(supportedCountries.getName());
        Truth.assertThat(country.getOfficialName()).isEqualTo(supportedCountries.getOfficialName());
        Truth.assertThat(country.getSupportedLanguages().size()).isEqualTo(1);
    }
}
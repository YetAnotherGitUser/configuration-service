package dev.ecosystem.configurationservice.application.configs.query.getconfigs;

import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.cached.GetConfigsCachedQuery;
import dev.ecosystem.configurationservice.application.exceptions.DatabaseDataException;
import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import dev.ecosystem.configurationservice.persistence.repository.ConfigurationParamsRepository;
import org.junit.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class GetConfigsQueryTest {

    private static final Long COUNTRY_ID = 369L;

    @InjectMocks
    private GetConfigsCachedQuery getConfigsQuery;

    @Mock
    private ConfigurationParamsRepository configurationParamsRepository;

//    @Test
//    public void testQuery() {
//        ConfigurationParam configurationParam = new ConfigurationParam();
//        List<ConfigurationParam> configurationParamList = List.of(configurationParam);
//        when(configurationParamsRepository.findAllByCountryId(COUNTRY_ID)).thenReturn(configurationParamList);
//
//        Set<Config> response = getConfigsQuery.execute(COUNTRY_ID);
//
//        Truth.assertThat(response).hasSize(1);
//        Config config = response.iterator().next();
//        Truth.assertThat(config.getId()).isEqualTo(configurationParam.getId());
//        Truth.assertThat(config.getCountryId()).isEqualTo(configurationParam.getCountryId());
//        Truth.assertThat(config.getKey()).isEqualTo(configurationParam.getKey());
//        Truth.assertThat(config.getValue()).isEqualTo(configurationParam.getValue());
//    }
}
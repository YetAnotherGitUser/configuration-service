package dev.ecosystem.configurationservice.application.configs.query.getconfigs;

import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import org.junit.jupiter.api.Test;

class ConfigTest {

    private static final Long COUNTRY_ID = 1L;
    private static final Long ID = 2L;
    private static final String KEY = "KEY";
    private static final String VALUE = "VALUE";

    @Test
    public void testConstructor() {
        ConfigurationParam configurationParam = new ConfigurationParam();
        configurationParam.setCountryId(COUNTRY_ID);
        configurationParam.setId(ID);
        configurationParam.setKey(KEY);
        configurationParam.setValue(VALUE);

        Config config = new Config(configurationParam);

        Truth.assertThat(config.getCountryId()).isEqualTo(COUNTRY_ID);
        Truth.assertThat(config.getId()).isEqualTo(ID);
        Truth.assertThat(config.getKey()).isEqualTo(KEY);
        Truth.assertThat(config.getValue()).isEqualTo(VALUE);
    }
}
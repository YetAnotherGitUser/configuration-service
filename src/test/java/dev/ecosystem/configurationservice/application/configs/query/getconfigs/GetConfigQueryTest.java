package dev.ecosystem.configurationservice.application.configs.query.getconfigs;

import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.cached.GetConfigCachedQuery;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.direct.GetConfigQuery;
import dev.ecosystem.configurationservice.application.exceptions.NotFoundException;
import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import dev.ecosystem.configurationservice.persistence.repository.ConfigurationParamsRepository;
import org.junit.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GetConfigQueryTest {

    private static final Long COUNTRY_ID = 5L;

    private static final String KEY = "KEY";

    @InjectMocks
    private GetConfigCachedQuery getConfigQuery;

    @Mock
    private ConfigurationParamsRepository configurationParamsRepository;

//    @Test
//    public void testQuery() {
//        ConfigurationParam param = new ConfigurationParam();
//        when(configurationParamsRepository.findByCountryIdAndKey(COUNTRY_ID, KEY)).thenReturn(Optional.of(param));
//
//        Config response = getConfigQuery.execute(COUNTRY_ID, KEY);
//
//        Truth.assertThat(response.getId()).isEqualTo(param.getId());
//        Truth.assertThat(response.getCountryId()).isEqualTo(param.getCountryId());
//        Truth.assertThat(response.getKey()).isEqualTo(param.getKey());
//        Truth.assertThat(response.getValue()).isEqualTo(param.getValue());
//    }
}
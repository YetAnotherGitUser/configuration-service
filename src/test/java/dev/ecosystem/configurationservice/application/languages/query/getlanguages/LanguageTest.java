package dev.ecosystem.configurationservice.application.languages.query.getlanguages;

import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.application.countries.query.getcountries.Language;
import dev.ecosystem.configurationservice.persistence.entity.SupportedLanguages;
import org.junit.jupiter.api.Test;

class LanguageTest {

    @Test
    public void testConstructor() {
        SupportedLanguages supportedLanguages = new SupportedLanguages();

        Language language = new Language(supportedLanguages);

        Truth.assertThat(language.getId()).isEqualTo(supportedLanguages.getId());
        Truth.assertThat(language.getCode()).isEqualTo(supportedLanguages.getCode());
        Truth.assertThat(language.getName()).isEqualTo(supportedLanguages.getName());
    }
}
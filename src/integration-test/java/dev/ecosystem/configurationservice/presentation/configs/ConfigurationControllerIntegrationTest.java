package dev.ecosystem.configurationservice.presentation.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.cached.GetConfigCachedQuery;
import dev.ecosystem.configurationservice.application.exceptions.NotFoundException;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.Config;
import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.OffsetDateTime;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@WebMvcTest(controllers = ConfigurationController.class)
@AutoConfigureMockMvc
public class ConfigurationControllerIntegrationTest {

    private static final Long ID = 5L;

    private static final Long USER_ID = 345L;

    private static final Long COUNTRY_ID = 4L;

    private static final String KEY = "common.default.language";

    private static final String VALUE = "VALUE";

    private static final OffsetDateTime CHANGE_DATE = OffsetDateTime.now();

    private static final OffsetDateTime CREATION_DATE = OffsetDateTime.now();

    @Autowired
    private  MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private GetConfigCachedQuery getConfigCachedQuery;

    @Test
    public void testExistingResource() throws Exception {
        ConfigurationParam configurationParam = new ConfigurationParam();
        configurationParam.setId(ID);
        configurationParam.setCountryId(COUNTRY_ID);
        configurationParam.setKey(KEY);
        configurationParam.setValue(VALUE);
        configurationParam.setChangedBy(USER_ID);
        configurationParam.setChangeDate(CHANGE_DATE);
        configurationParam.setCreationDate(CREATION_DATE);

        Config mockedResponse = new Config(configurationParam);

        when(getConfigCachedQuery.execute(eq(COUNTRY_ID), eq(KEY))).thenReturn(mockedResponse);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/config/common.default.language")
                .header("countryId", COUNTRY_ID);
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        Truth.assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);
        Truth.assertThat(mvcResult.getResponse().getContentType()).isEqualTo("application/json");
        Truth.assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo(objectMapper.writeValueAsString(mockedResponse));
    }

    @Test
    public void testNonExistingResource() throws Exception {
        when(getConfigCachedQuery.execute(eq(COUNTRY_ID), eq(KEY))).thenThrow(NotFoundException.class);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/config/common.default.language")
                .header("countryId", COUNTRY_ID);
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        Truth.assertThat(mvcResult.getResponse().getStatus()).isEqualTo(404);
        Truth.assertThat(mvcResult.getResponse().getContentType()).isEqualTo(null);
        Truth.assertThat(mvcResult.getResponse().getContentAsString()).isEmpty();
    }
}

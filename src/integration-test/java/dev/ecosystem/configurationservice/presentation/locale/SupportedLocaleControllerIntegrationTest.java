package dev.ecosystem.configurationservice.presentation.locale;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.application.countries.query.getcountries.Country;
import dev.ecosystem.configurationservice.application.countries.query.getcountries.cached.GetLocaleCachedQuery;
import dev.ecosystem.configurationservice.application.exceptions.DatabaseDataException;
import dev.ecosystem.configurationservice.persistence.entity.SupportedCountries;
import dev.ecosystem.configurationservice.persistence.entity.SupportedLanguages;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.OffsetDateTime;
import java.util.Set;

import static org.mockito.Mockito.when;

@WebMvcTest(controllers = SupportedLocaleController.class)
@AutoConfigureMockMvc
public class SupportedLocaleControllerIntegrationTest {

    private static final Long ID = 5L;

    private static final Long USER_ID = 345L;

    private static final String CODE = "EU";

    private static final String NAME = "Europa";

    private static final String OFFICIAL_NAME = "EUROPEAN UNION";

    private static final OffsetDateTime CHANGE_DATE = OffsetDateTime.now();

    private static final OffsetDateTime CREATION_DATE = OffsetDateTime.now();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private GetLocaleCachedQuery getLocaleCachedQuery;

    @Test
    public void testSingleExistingResource() throws Exception {
        SupportedLanguages supportedLanguages = new SupportedLanguages();
        SupportedCountries supportedCountries = new SupportedCountries();
        supportedCountries.setId(ID);
        supportedCountries.setCode(CODE);
        supportedCountries.setName(NAME);
        supportedCountries.setOfficialName(OFFICIAL_NAME);
        supportedCountries.setChangedBy(USER_ID);
        supportedCountries.setChangeDate(CHANGE_DATE);
        supportedCountries.setCreationDate(CREATION_DATE);
        supportedCountries.setLanguages(Set.of(supportedLanguages));

        Country country = new Country(supportedCountries);

        Set<Country> mockedResponse = Set.of(country);

        when(getLocaleCachedQuery.execute()).thenReturn(mockedResponse);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/countries");
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        Truth.assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);
        Truth.assertThat(mvcResult.getResponse().getContentType()).isEqualTo("application/json");
        Truth.assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo(objectMapper.writeValueAsString(mockedResponse));
    }

    @Test
    public void testNonExistingResource() throws Exception {
        when(getLocaleCachedQuery.execute()).thenThrow(DatabaseDataException.class);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/countries");
        MvcResult mvcResult = mockMvc.perform(requestBuilder).andReturn();

        Truth.assertThat(mvcResult.getResponse().getStatus()).isEqualTo(500);
        Truth.assertThat(mvcResult.getResponse().getContentType()).isEqualTo(null);
        Truth.assertThat(mvcResult.getResponse().getContentAsString()).isEmpty();
    }
}

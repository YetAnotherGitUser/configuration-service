package dev.ecosystem.configurationservice.persistence.repository;

import com.google.common.truth.Truth;
import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.Optional;

@DataJpaTest
public class ConfigurationParamsRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private ConfigurationParamsRepository configurationParamsRepository;

    @Test
    public void testFindByCountryIdAndKeyWhenEmpty() {
        Optional<ConfigurationParam> resultFromDb = configurationParamsRepository.findByCountryIdAndKey(5L, "myKey");

        Truth.assertThat(resultFromDb.isEmpty()).isTrue();
    }

    @Test
    public void testFindAllByCountryIdWhenEmpty() {
        List<ConfigurationParam> resultFromDb = configurationParamsRepository.findAllByCountryId(6L);

        Truth.assertThat(resultFromDb).isEmpty();
    }
}

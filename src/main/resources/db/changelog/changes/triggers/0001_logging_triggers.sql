CREATE TRIGGER supported_countries_logger
AFTER INSERT OR UPDATE OR DELETE
ON supported_countries FOR EACH ROW
EXECUTE PROCEDURE log_supported_countries ();

CREATE TRIGGER supported_countries_autofill
BEFORE INSERT OR UPDATE
ON supported_countries FOR EACH ROW
EXECUTE PROCEDURE autofill_change_dates ();


CREATE TRIGGER supported_languages_logger
AFTER INSERT OR UPDATE OR DELETE
ON supported_languages FOR EACH ROW
EXECUTE PROCEDURE log_supported_languages ();

CREATE TRIGGER supported_languages_autofill
BEFORE INSERT OR UPDATE
ON supported_languages FOR EACH ROW
EXECUTE PROCEDURE autofill_change_dates ();


CREATE TRIGGER config_params_logger
AFTER INSERT OR UPDATE OR DELETE
ON config_params FOR EACH ROW
EXECUTE PROCEDURE log_config_params ();

CREATE TRIGGER config_params_autofill
BEFORE INSERT OR UPDATE
ON config_params FOR EACH ROW
EXECUTE PROCEDURE autofill_change_dates ();
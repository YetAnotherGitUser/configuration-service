CREATE TABLE supported_countries
(
    id bigint GENERATED ALWAYS AS IDENTITY,
    name text NOT NULL,
    official_name text NOT NULL,
    code text NOT NULL,
    creation_date timestamp with time zone default current_timestamp,
    change_date timestamp with time zone NOT NULL,
    changed_by bigint NOT NULL,
    UNIQUE  (name),
    UNIQUE  (code),
    PRIMARY KEY (id)
);

CREATE TABLE supported_countries_log
(
    id bigint NOT NULL,
    name text,
    official_name text NOT NULL,
    code text,
    creation_date timestamp with time zone,
    change_date timestamp with time zone,
    changed_by bigint,
    change_type text,
    db_user text default session_user
);

CREATE TABLE supported_languages
(
    id bigint GENERATED ALWAYS AS IDENTITY,
    country_id bigint NOT NULL,
    name text NOT NULL,
    code text NOT NULL,
    creation_date timestamp with time zone default current_timestamp,
    change_date timestamp with time zone NOT NULL,
    changed_by bigint NOT NULL,
    UNIQUE (country_id, code),
    UNIQUE (country_id, name),
    PRIMARY KEY (id),
    CONSTRAINT "reference to supported countries" FOREIGN KEY (country_id)
        REFERENCES supported_countries (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION

);

CREATE TABLE supported_languages_log
(
    id bigint NOT NULL,
    country_id bigint,
    name text,
    code text,
    creation_date timestamp with time zone,
    change_date timestamp with time zone,
    changed_by bigint,
    change_type text,
    db_user text default session_user
);


CREATE TABLE config_params
(
    id bigint GENERATED ALWAYS AS IDENTITY,
    country_id bigint NOT NULL,
    param_name text NOT NULL,
    param_value text NOT NULL,
    creation_date timestamp with time zone default current_timestamp,
    change_date timestamp with time zone NOT NULL,
    changed_by bigint NOT NULL,
    UNIQUE  (param_name),
    PRIMARY KEY (id),
    CONSTRAINT "reference to supported countries" FOREIGN KEY (country_id)
        REFERENCES supported_countries (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION

);

CREATE TABLE config_params_log
(
    id bigint NOT NULL,
    country_id bigint,
    param_name text,
    param_value text,
    creation_date timestamp with time zone,
    change_date timestamp with time zone,
    changed_by bigint,
    change_type text,
    db_user text default session_user
);
CREATE OR REPLACE FUNCTION log_supported_countries() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        INSERT INTO supported_countries_log(change_type, id, name, official_name, code, creation_date, change_date, changed_by)
        VALUES (TG_OP, NEW.*);
        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN
        OLD.change_date := current_timestamp;
        INSERT INTO supported_countries_log(change_type, id, name, official_name, code, creation_date, change_date, changed_by)
        VALUES (TG_OP, OLD.*);
        RETURN OLD;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_supported_languages() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        INSERT INTO supported_languages_log(change_type, id, country_id, name, code, creation_date, change_date, changed_by)
        VALUES (TG_OP, NEW.*);
        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN
        OLD.change_date := current_timestamp;
        INSERT INTO supported_languages_log(change_type, id, country_id, name, code, creation_date, change_date, changed_by)
        VALUES (TG_OP, OLD.*);
        RETURN OLD;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION log_config_params() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        INSERT INTO config_params_log(change_type, id, country_id, param_name, param_value, creation_date, change_date, changed_by)
        VALUES (TG_OP, NEW.*);
        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN
        OLD.change_date := current_timestamp;
        INSERT INTO config_params_log(change_type, id, country_id, param_name, param_value, creation_date, change_date, changed_by)
        VALUES (TG_OP, OLD.*);
        RETURN OLD;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION autofill_change_dates() RETURNS TRIGGER AS $$
BEGIN
    NEW.change_date := current_timestamp;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
INSERT INTO supported_countries (name, official_name, code, changed_by) VALUES ('Global', 'Common to all coutries', 'GLOBAL', 0);

INSERT INTO supported_languages (country_id, name, code, changed_by) VALUES ((SELECT id FROM supported_countries WHERE code = 'GLOBAL'), 'Lithuanian', 'lit', 0);
INSERT INTO supported_languages (country_id, name, code, changed_by) VALUES ((SELECT id FROM supported_countries WHERE code = 'GLOBAL'), 'English', 'eng', 0);
INSERT INTO supported_languages (country_id, name, code, changed_by) VALUES ((SELECT id FROM supported_countries WHERE code = 'GLOBAL'), 'Russian', 'rus', 0);
INSERT INTO supported_languages (country_id, name, code, changed_by) VALUES ((SELECT id FROM supported_countries WHERE code = 'GLOBAL'), 'Polish', 'pol', 0);

INSERT INTO config_params (country_id, param_name, param_value, changed_by) VALUES ((SELECT id FROM supported_countries WHERE code = 'GLOBAL'), 'common.default.language', (SELECT id FROM supported_languages WHERE code = 'eng'), 0);
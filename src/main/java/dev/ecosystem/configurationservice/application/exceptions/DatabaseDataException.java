package dev.ecosystem.configurationservice.application.exceptions;

public class DatabaseDataException extends RuntimeException {
    public DatabaseDataException(String message) {
        super(message);
    }
}
package dev.ecosystem.configurationservice.application.countries.query.getcountries;

import dev.ecosystem.configurationservice.persistence.entity.SupportedCountries;
import lombok.Getter;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
public class Country implements Serializable {

    private Long id;

    private String name;

    private String officialName;

    private String code;

    private Set<Language> supportedLanguages;

    public Country(SupportedCountries supportedCountries) {
        this.id = supportedCountries.getId();
        this.name = supportedCountries.getName();
        this.officialName = supportedCountries.getOfficialName();
        this.code = supportedCountries.getCode();
        this.supportedLanguages = supportedCountries
                .getLanguages()
                .stream()
                .map(Language::new)
                .collect(Collectors.toSet());
    }
}

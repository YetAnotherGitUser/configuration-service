package dev.ecosystem.configurationservice.application.countries.query.getcountries.direct;

import dev.ecosystem.configurationservice.application.countries.query.getcountries.Country;
import dev.ecosystem.configurationservice.application.exceptions.DatabaseDataException;
import dev.ecosystem.configurationservice.persistence.entity.SupportedCountries;
import dev.ecosystem.configurationservice.persistence.repository.SupportedCountriesRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class GetLocaleQuery {

    private final SupportedCountriesRepository supportedCountriesRepository;

    @Cacheable(cacheNames = "supported_locale")
    public Set<Country> executeFromCache() {
        return null;
    }

    @CachePut(cacheNames = "supported_locale")
    public Set<Country> executeFromDatabase() {
        log.debug("Preparing to execute query for supported locale fetching");
        List<SupportedCountries> supportedCountries = supportedCountriesRepository.findAll();

        if(supportedCountries.isEmpty()) {
            String errorMessage = "Couldn't find any countries, check database";
            log.error(errorMessage);
            throw new DatabaseDataException(errorMessage);
        }

        supportedCountries.forEach(country -> {
            if(country.getLanguages().isEmpty()) {
                String errorMessage = "Couldn't find any language for ["
                        + country.getId()
                        + "] country id, check database";
                log.error(errorMessage);
                throw new DatabaseDataException(errorMessage);
            }
        });

        return supportedCountries
                    .stream()
                    .map(Country::new)
                    .collect(Collectors.toSet());
    }
}

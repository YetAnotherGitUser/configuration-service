package dev.ecosystem.configurationservice.application.countries.query.getcountries.cached;

import dev.ecosystem.configurationservice.application.countries.query.getcountries.Country;
import dev.ecosystem.configurationservice.application.countries.query.getcountries.direct.GetLocaleQuery;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;

@Slf4j
@Service
@AllArgsConstructor
public class GetLocaleCachedQuery {

    private final GetLocaleQuery getLocaleQuery;

    public Set<Country> execute() {
        Set<Country> supportedLocale = getLocaleQuery.executeFromCache();
        if (supportedLocale == null) {
            log.info("Cache is empty, fetching supported locale from database");
            supportedLocale = getLocaleQuery.executeFromDatabase();
        }
        return supportedLocale;
    }
}

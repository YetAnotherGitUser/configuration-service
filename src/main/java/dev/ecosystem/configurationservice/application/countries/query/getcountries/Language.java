package dev.ecosystem.configurationservice.application.countries.query.getcountries;

import dev.ecosystem.configurationservice.persistence.entity.SupportedLanguages;
import lombok.Getter;

import java.io.Serializable;

@Getter
public class Language implements Serializable {
    private Long id;

    private String name;

    private String code;

    public Language(SupportedLanguages supportedLanguages) {
        this.id = supportedLanguages.getId();
        this.name = supportedLanguages.getName();
        this.code = supportedLanguages.getCode();
    }
}

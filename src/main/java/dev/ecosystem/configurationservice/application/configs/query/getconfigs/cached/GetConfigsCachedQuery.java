package dev.ecosystem.configurationservice.application.configs.query.getconfigs.cached;

import dev.ecosystem.configurationservice.application.configs.query.getconfigs.Config;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.direct.GetConfigsQuery;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;

@Slf4j
@Service
@AllArgsConstructor
public class GetConfigsCachedQuery {

    private final GetConfigsQuery getConfigsQuery;

    public Set<Config> execute(Long countryId) {
        Set<Config> configs = getConfigsQuery.executeFromCache(countryId);
        if (configs == null) {
            log.info("Cache is empty, fetching configs from database");
            configs = getConfigsQuery.executeFromDatabase(countryId);
        }
        return configs;
    }
}

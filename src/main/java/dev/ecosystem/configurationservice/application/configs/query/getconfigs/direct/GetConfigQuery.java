package dev.ecosystem.configurationservice.application.configs.query.getconfigs.direct;

import dev.ecosystem.configurationservice.application.configs.query.getconfigs.Config;
import dev.ecosystem.configurationservice.application.exceptions.NotFoundException;
import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import dev.ecosystem.configurationservice.persistence.repository.ConfigurationParamsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class GetConfigQuery {

    private final ConfigurationParamsRepository configurationParamsRepository;

    @Cacheable(cacheNames = "config")
    public Config executeFromCache(Long countryId, String paramKey) {
        return null;
    }

    @CachePut(cacheNames = "config")
    public Config executeFromDatabase(Long countryId, String paramKey) {
        log.debug("Preparing to execute query for config fetching");
        ConfigurationParam configurationParam  = configurationParamsRepository
                .findByCountryIdAndKey(countryId, paramKey)
                .orElseGet(() -> {
                    String errorMessage = "Configuration param with such country id ["
                            + countryId.toString()
                            + "] and key ["
                            + paramKey
                            + "] not found.";
                    log.error(errorMessage);
                    throw new NotFoundException(errorMessage);
                });
        return new Config(configurationParam);
    }
}

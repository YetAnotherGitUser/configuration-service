package dev.ecosystem.configurationservice.application.configs.query.getconfigs.cached;

import dev.ecosystem.configurationservice.application.configs.query.getconfigs.Config;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.direct.GetConfigQuery;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class GetConfigCachedQuery {

    private final GetConfigQuery getConfigQuery;

    public Config execute(Long countryId, String paramKey) {
        Config config = getConfigQuery.executeFromCache(countryId, paramKey);
        if (config == null) {
            log.info("Cache is empty, fetching config from database");
            config = getConfigQuery.executeFromDatabase(countryId, paramKey);
        }
        return config;
    }
}

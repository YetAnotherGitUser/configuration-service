package dev.ecosystem.configurationservice.application.configs.query.getconfigs.direct;

import dev.ecosystem.configurationservice.application.configs.query.getconfigs.Config;
import dev.ecosystem.configurationservice.application.exceptions.DatabaseDataException;
import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import dev.ecosystem.configurationservice.persistence.repository.ConfigurationParamsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class GetConfigsQuery {

    private final ConfigurationParamsRepository configurationParamsRepository;

    @Cacheable(cacheNames = "configs", key = "'country_id_'.concat(#countryId)")
    public Set<Config> executeFromCache(Long countryId) {
        return null;
    }

    @CachePut(cacheNames = "configs", key = "'country_id_'.concat(#countryId)")
    public Set<Config> executeFromDatabase(Long countryId) {
        log.debug("Preparing to execute query for configs fetching");
        List<ConfigurationParam> fetchedConfigs = configurationParamsRepository.findAllByCountryId(countryId);
        if (fetchedConfigs.isEmpty()) {
            String errorMessage = "Couldn't find any configurations for [" + countryId + "] country ID";
            log.error(errorMessage);
            throw new DatabaseDataException(errorMessage);
        } else {
            return fetchedConfigs
                    .stream()
                    .map(Config::new)
                    .collect(Collectors.toSet());
        }
    }
}

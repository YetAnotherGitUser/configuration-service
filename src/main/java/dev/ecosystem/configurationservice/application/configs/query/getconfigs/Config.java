package dev.ecosystem.configurationservice.application.configs.query.getconfigs;

import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import lombok.Getter;

import java.io.Serializable;

@Getter
public class Config implements Serializable {

    private Long id;

    private Long countryId;

    private String key;

    private String value;

    public Config(ConfigurationParam configurationParam) {
        this.id = configurationParam.getId();
        this.countryId = configurationParam.getCountryId();
        this.key = configurationParam.getKey();
        this.value = configurationParam.getValue();
    }
}

package dev.ecosystem.configurationservice;

import dev.ecosystem.ipcomponent.common.ip.Ipify;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Launcher {

	public static void main(String[] args) {
		Ipify.setIpForEureka();
		SpringApplication.run(Launcher.class, args);
	}
}
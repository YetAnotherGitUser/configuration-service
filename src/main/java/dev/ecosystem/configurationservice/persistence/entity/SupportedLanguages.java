package dev.ecosystem.configurationservice.persistence.entity;

import lombok.Data;
import org.springframework.data.redis.core.index.Indexed;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "SUPPORTED_LANGUAGES")
public class SupportedLanguages {

    @Id
    @Column(name = "ID")
    private Long id;

    @Indexed
    @Column(name = "COUNTRY_ID")
    private Long countryId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CODE")
    private String code;

    @Column(name = "CREATION_DATE", insertable = false, updatable = false)
    private OffsetDateTime creationDate;

    @Column(name = "CHANGE_DATE", insertable = false, updatable = false)
    private OffsetDateTime changeDate;

    @Column(name = "CHANGED_BY")
    private Long changedBy;
}

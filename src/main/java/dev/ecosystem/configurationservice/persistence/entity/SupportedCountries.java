package dev.ecosystem.configurationservice.persistence.entity;

import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.OffsetDateTime;
import java.util.Set;

@Data
@Entity
@Table(name = "SUPPORTED_COUNTRIES")
public class SupportedCountries {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "OFFICIAL_NAME")
    private String officialName;

    @Column(name = "CODE")
    private String code;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "COUNTRY_ID")
    private Set<SupportedLanguages> languages;

    @LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "COUNTRY_ID")
    private Set<ConfigurationParam> configurations;

    @Column(name = "CREATION_DATE", insertable = false, updatable = false)
    private OffsetDateTime creationDate;

    @Column(name = "CHANGE_DATE", insertable = false, updatable = false)
    private OffsetDateTime changeDate;

    @Column(name = "CHANGED_BY")
    private Long changedBy;
}

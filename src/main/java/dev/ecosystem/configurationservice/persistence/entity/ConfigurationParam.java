package dev.ecosystem.configurationservice.persistence.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "CONFIG_PARAMS")
public class ConfigurationParam {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "COUNTRY_ID")
    private Long countryId;

    @Column(name = "PARAM_NAME")
    private String key;

    @Column(name = "PARAM_VALUE")
    private String value;

    @Column(name = "CREATION_DATE", insertable = false, updatable = false)
    private OffsetDateTime creationDate;

    @Column(name = "CHANGE_DATE", insertable = false, updatable = false)
    private OffsetDateTime changeDate;

    @Column(name = "CHANGED_BY")
    private Long changedBy;
}

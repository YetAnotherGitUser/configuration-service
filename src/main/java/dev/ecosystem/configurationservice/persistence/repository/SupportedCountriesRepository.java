package dev.ecosystem.configurationservice.persistence.repository;

import dev.ecosystem.configurationservice.persistence.entity.SupportedCountries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupportedCountriesRepository extends JpaRepository<SupportedCountries, Long> {
}

package dev.ecosystem.configurationservice.persistence.repository;

import dev.ecosystem.configurationservice.persistence.entity.ConfigurationParam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConfigurationParamsRepository extends JpaRepository<ConfigurationParam, Long> {

    Optional<ConfigurationParam> findByCountryIdAndKey(@Param("COUNTRY_ID") Long countryId,
                                                       @Param("PARAM_NAME") String key);

    List<ConfigurationParam> findAllByCountryId(@Param("COUNTRY_ID") Long countryId);
}

package dev.ecosystem.configurationservice.presentation.configs;

import dev.ecosystem.configurationservice.application.configs.query.getconfigs.cached.GetConfigCachedQuery;
import dev.ecosystem.configurationservice.application.exceptions.NotFoundException;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.Config;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class ConfigurationController {

    private final GetConfigCachedQuery query;

    @GetMapping(value = "/config/{key}", produces = "application/json")
    public ResponseEntity<Config> getConfig(@RequestHeader("countryId") Long countryId, @PathVariable("key") String key) {
        Config config;
        HttpStatus responseStatus;
        try {
            config = query.execute(countryId, key);
            responseStatus = HttpStatus.OK;
        } catch (NotFoundException e) {
            config = null;
            responseStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(config, responseStatus);
    }
}

package dev.ecosystem.configurationservice.presentation.configs;

import dev.ecosystem.configurationservice.application.configs.query.getconfigs.Config;
import dev.ecosystem.configurationservice.application.configs.query.getconfigs.cached.GetConfigsCachedQuery;
import dev.ecosystem.configurationservice.application.exceptions.DatabaseDataException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@AllArgsConstructor
public class ConfigurationsController {

    private final GetConfigsCachedQuery query;

    @GetMapping(value = "/configs", produces = "application/json")
    public ResponseEntity<Set<Config>> getConfigs(@RequestHeader("countryId") Long countryId) {
        Set<Config> configList;
        HttpStatus responseStatus;
        try {
            configList = query.execute(countryId);
            responseStatus = HttpStatus.OK;
        } catch (DatabaseDataException e) {
            responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            configList = null;
        }
        return new ResponseEntity<>(configList, responseStatus);
    }
}

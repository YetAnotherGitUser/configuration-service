package dev.ecosystem.configurationservice.presentation.locale;

import dev.ecosystem.configurationservice.application.countries.query.getcountries.cached.GetLocaleCachedQuery;
import dev.ecosystem.configurationservice.application.exceptions.DatabaseDataException;
import dev.ecosystem.configurationservice.application.countries.query.getcountries.Country;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@AllArgsConstructor
public class SupportedLocaleController {

    private final GetLocaleCachedQuery query;

    @GetMapping(value = "/countries", produces = "application/json")
    public ResponseEntity<Set<Country>> getSupportedCountries() {
        Set<Country> supportedCountries;
        HttpStatus responseStatus;
        try {
            supportedCountries = query.execute();
            responseStatus = HttpStatus.OK;
        } catch (DatabaseDataException e) {
            responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            supportedCountries = null;
        }
        return new ResponseEntity<>(supportedCountries, responseStatus);
    }
}